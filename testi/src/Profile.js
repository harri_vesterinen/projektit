import React from 'react';

class Profile extends React.Component {
    
    render() {

        // funktio, jolla järjestetään samat kupongit ryhmiin taulukkoon (kuponki id:n mukaan)
        //    Asetetaan funktio muuttujaan groupCoupons
        // tuloksena esim. kuponkeja X 3 kpl, kuponkeja Y 5 kpl jne ryhmiteltynä taulukkoon
        let groupCoupons = function( groupedCoupons, currentCoupon ) {

            // onko kuponki voimassa
            if( currentCoupon.validDue >= Date.now() ) {
                // löytyykö kuponki jo ryhmitellystä kuponkitaulukosta
                let index = groupedCoupons.findIndex( coupon => coupon.id === currentCoupon.id )
                // indexiin palautuu < 0, jos indexiä ei vielä löydy taulukosta. Muutoin posit. luku
                if( index < 0 ) {
                    currentCoupon.count = 1; // ei löydy vielä, asetetaan count arvoksi 1
                    // lisätään uusi kuponki groupedCoupons-taulukkoon loppuun
                    groupedCoupons.push( currentCoupon );
                } else {
                    // jos löytyy jo taulukosta, lisätään kupongin countia yhdellä:
                    groupedCoupons[index].count = groupedCoupons[index].count + 1;
                    // tutkitaan vielä, onko currentCoupon:n validDue aiemmin, kuin jo taulukossa
                    //  oleva validDue (eli varhaiten vanhenevan kupongin validDue tulee taulukkoon)
                    if( groupedCoupons[index].validDue > currentCoupon.validDue ) {
                        groupedCoupons[index].validDue = currentCoupon.validDue;
                        // lisäksi laitetaan talteen taulukkoon vielä currentCoupon:n 
                        // lunastusaika claimed (jos sitä tarttee jossain):
                        groupedCoupons[index].claimed = currentCoupon.claimed;
                    }
                }
            }
            return groupedCoupons;
        }

        // kutsutaan yo. groupCoupons-muuttujafunktiota: 
        //   se menee parametrina coupons-taulukon reduce-funktioon tyhjän taulukon kanssa,
        //   reduce pyöräyttää koko coupons-taulukon läpi ja kutsuu aina groupCoupons-funktiota
        //   muuttujaan coupons palautuu muodostettu taulukko
        // Alkuperäinen lunastettavien kuponkien taulukko tulee ylemmän tason App-komponentilta
        //   propsin kautta (props.coupons)
        let coupons = this.props.coupons.reduce( groupCoupons, [] );

        // mapataan coupons-taulukko ja tulostetaan se
        let rows = coupons.map(coupon => {

            // muutetaan coupon:n validDue millisekuntiluku oikeaksi päivämääräksi:
            let validDue = new Date(coupon.validDue).toDateString();

            return (
                <div className="coupon" key={coupon.id}>
                    <div className="coupon__offer">
                        <div className="coupon__offerName">{coupon.name}</div>
                        <div className="coupon_offerDesc">{coupon.desc}</div>
                    </div>
                    <div className="coupon__prices">
                        <div className="coupon__newPrice">{coupon.newPrice.toFixed(2)}</div>
                        <div className="coupon__oldPrice">{coupon.oldPrice.toFixed(2)}</div>
                    </div>
                    <div className="coupon__button">
                        <div className="coupon__price">{coupon.count} pcs</div>
                        <div className="coupon__claim">Valid due {validDue}</div>
                    </div>
                </div>
            );
        }
        );

        return (
            // Huom: sama kuin <React.Fragment> -tagi alla oleva <>
            <>          
                <div className="header">
                    <h1>Profile</h1>
                </div>
                <div className="content">
                    {rows}
                </div>
            </>         
            // Huom: sama kuin </React.Fragment> -lopetustagi tässä oleva </>
        );
    }
}

export default Profile;