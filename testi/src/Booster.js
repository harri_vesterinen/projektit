import React from 'react';

// Booster-funktio
function Booster(props) {
    return (
      <div className="booster">
        {props.boost} Burgers / Click
      </div>
    );
}

export default Booster;