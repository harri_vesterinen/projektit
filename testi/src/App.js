import React, {Component} from 'react';
import { Route, BrowserRouter as Router } from 'react-router-dom';

import allCoupons from './allCoupons';

import Menu from './Menu';
import Game from './Game';
import Coupons from './Coupons';
import Profile from './Profile';

import './App.css';

// Clicker-luokka
class Clicker extends Component {

  constructor(props) {
    super(props);
    // haetaan localStoragesta clicks:
    let clicks = parseInt(localStorage.getItem("clicks")); // muunnettava int:ksi, koska localStoragessa merkkinä
    // löytyykö arvoa localStoragesta:
    clicks = clicks ? clicks : 0;
    // haetaan localStoragesta coupons: 
    let coupons = localStorage.getItem("coupons");
    coupons = coupons ? JSON.parse(coupons) : []; // muunnetaan JSON string -> objektiksi, jos löytyy localStoragesta

    this.state = {
      clicks: clicks, // laskettu yllä
      coupons: coupons, // laskettu yllä
      claimableCoupons: 0, // lunastettavien kuponkien määrä
      countUpdateValue: 0  // missä kuponkien raja-arvokohdassa on päivitetty viimeksi
                           // lunastettavien kuponkien määrää etusivulla
    }
    // alemmille komponenteille välitettävät ns. callback-funktiot pitää 'bindata'
    // tässä ylemmän tason komponentin construktorissa ao. tavalla: (muuten
    // alemman tason komponentti ei löydä ko. funktiota!)
    this.setClicks = this.setClicks.bind(this);
    this.claimCoupon = this.claimCoupon.bind(this);
    this.updateCouponCount = this.updateCouponCount.bind(this);
  }

  // kutsutaan heti, kun Clicker-komponentti on kytketty (mounted) DOM-puuhun. 
  // Ks. lisää 'component'-alkuisia eventtejä kirjoittamalla 'component' koodiin.
  // Tässä eventissä kutsutaan Clicker-luokan updateCouponCount-funktiota:
  componentDidMount() {
    this.updateCouponCount(this.state.clicks);
  }

  // funktio, jolla päivitetään lunastettavien kuponkien määrää etusivun ikonin päällä
  // Tätä kutsutaan vain tarvittaessa, ei joka kerta, kun klikataan burgeria (optimointi).
  // Hyödynnetään state-muuttujissa claimableCoupons ja countUpdateValue -muuttujia
  updateCouponCount(clicks) {
    let coupons = 0; // apumuuttuja, lunastettavien kuponkien määrä
    let updateValue = this.state.countUpdateValue; // apumuuttuja: kuponkien raja-arvo, kun viimeksi
    // on päivitetty etusivulla lunastettavien kuponkien määrää,
    // eli pitääkö päivittää kuponkien määrää etusivulla
    // käydään läpi kaikki kupongit
    allCoupons.forEach( coupon => {
      // jos kupongin hinta <= clicks-määrä -> on lunastettavissa
      if ( coupon.price <= clicks ) {
        // päivitä päivitettävien kuponkien määrää
        coupons++;
      }
      //      25            37              50          75
      // ------------------------------------------------------------->
      //      ^             ^               ^           ^
      //  updateValue     clicks          coupon      coupon
      if ( (updateValue < clicks && coupon.price > updateValue) ||
            (coupon.price > clicks && coupon.price < updateValue) ) {
        updateValue = coupon.price;
      }
      // päivitetään state-muuttujiin tiedot
      this.setState({
        claimableCoupons: coupons,
        countUpdateValue: updateValue
      });
    } );
  }

  // klikattujen burgerien määrän päivitysfunktio: oltava App-komponentissa (ns. callback-funktio)
  // - funktio välitetään <Game>-komponentille
  setClicks(clicks) {
    this.setState({
      clicks: clicks
    });
    // onko clicks suurempi/yhtä suuri kuin statessa oleva lunastettujen kuponkien raja-arvo countUpdateValue
    if ( clicks >= this.state.countUpdateValue ) {
      // jos on -> päivitä lunastettavien kuponkien määrä etusivulle
      this.updateCouponCount(clicks);
    }
    // tallennetaan localStorageen clicksien määrä:
    localStorage.setItem("clicks", clicks);
  }

  // kupongin lunastusfunktio: välitetään <Coupons>-komponentille (ns. callback-funktio)
  claimCoupon(couponId) {
    let filteredCoupons = allCoupons.filter( offer => offer.id === couponId ); // hae lunastettu kuponki id:llä
    let selectedCoupon = Object.assign( {}, filteredCoupons[0] ); // kopioidaan lunastettu kuponki uuteen kuponkiolioon
    // lisätään kopioon uudet tietokentät: (eivät siis mene alkuperäisen allCoupons-taulukon kuponkiolioon)
    selectedCoupon.claimed = Date.now(); // lunastushetki
    // voimassa kaksi viikkoa lunastushetkestä: 14vrk * 24h * 60min * 60sek * 1000msek
    selectedCoupon.validDue = Date.now() + 14 * 24 * 60 * 60 * 1000;
    // vähennetään klikatuista burgereista lunastettavan kupongin hinta:
    let clicks = this.state.clicks;
    clicks = clicks - selectedCoupon.price;
    // kopioi staten coupons-taulukko uuteen taulukkomuuttujaan:
    let coupons = this.state.coupons.slice();
    // lisätään coupons-taulukkokopion loppuun valittu kuponkiolio:
    coupons.push(selectedCoupon);
    // päivitetään state-muuttujat:
    this.setState({
      clicks: clicks,
      coupons: coupons
    });
    // päivitä lunastettavien kuponkien määrä etusivulle
    this.updateCouponCount(clicks);
    // tallennetaan localStorageen clicks ja coupons:
    localStorage.setItem("clicks", clicks);
    // coupons on taulukko-olio, jota setItem ei osaa tallettaa localStorageen.
    // Siksi käytettävä JSON.stringify() -funktiota, joka muuntaa string-muotoon:
    localStorage.setItem("coupons", JSON.stringify(coupons) );
  }
  
  render() {
    return (
      <Router>
        <div className="clicker">
          <Route path="/" exact render={ props => (
            <Game clicks={this.state.clicks} setClicks={this.setClicks} />
          )} />
          <Route path="/coupons" render={ props => (
            <Coupons clicks={this.state.clicks} claimCoupon={this.claimCoupon} />
          )} />
          <Route path="/profile" render={ props => (
            <Profile coupons={this.state.coupons} />
          )} />
          <Menu claimableCoupons={this.state.claimableCoupons} />
        </div>
      </Router>
    );
  }
}

export default Clicker;