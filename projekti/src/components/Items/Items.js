import React from 'react';

// Link tarvitaan, jotta FloatingButton-napin painaminen voidaan ohjata
//  uuden laskutiedon syöttölomakkeelle.
import { Link } from 'react-router-dom';

import Kulukortti from '../Kulukortti/Kulukortti';
import Content from '../Content/Content';
import { FloatingButton } from '../buttons'; // Toimintonapit buttonseista Items-komponenttiin
// Napit on määritetty tiedostossa: components/buttons/index.js, sillä on
// tavallinen 'Button' sekä 'FloatingButton'.
// huom: yllä ei tarvitse laittaa ../buttons/index -tiedoston nimeä importtiin,
// koska webpack hakee automaattisesti buttons-kansiosta tiedoston index.js,
// jos sellainen löytyy. Voisi laittaa myös index:n perään, mutta se ei ole
// välttämätöntä tässä tapauksessa.

function Items(props) {

  // testidatan mappays, yksittäinen kulukortti tulee muuttujaan 'invoice'
  // data-taulukko välitetään propsina ylemmältä komponentilta App.js (on siellä state-muuttujassa).
  let rows = props.data.map( invoice => {
    return (
      <Kulukortti data={invoice} key={invoice.id} /> // välitetään 'invoice' eli yksittäinen kulukortti propsina
      // pitää antaa myös key-parametri, jotta jokaisella invoicella on yksilöivä tunnus
    );
  }
  );

  // Alla secondary-parametri laittaa secondary-määreen päälle buttons.css:stä
  // Tällöin napin väri muuttuu oranssiksi. Ilman secondary-parametria nappi on sininen.
  // Jos haluaa disabloida napin (näkyy harmaana mutta ei tee mitään), parametrina disabled
  return (
    <Content>
      {rows}
      <Link to="/add"><FloatingButton secondary>+</FloatingButton></Link>
    </Content>
  );
}

export default Items;