import React from 'react';
import { Line } from 'react-chartjs-2'; // Line-graafi
import { Doughnut } from 'react-chartjs-2'; // Donitsi-graafi
import stringHash from 'string-hash'; // hajautusfunktio värien luontiin

import Content from '../Content/Content';

import './Stats.css';

function Stats(props) {

  // ******* Donitsi-kuvio:
  // Piirretään kulutyypin (x-akseli) mukaan kulutyypin kokonaissumma (y-akseli).
  // Ryhmitellään kulukortit kulutyypin mukaan groupedData-taulukkoon. Taulukko on muotoa:
  //  [{ tyyppi: currentItem.tyyppi, summa: currentItem.summa },
  //   { tyyppi: currentItem.tyyppi, summa: currentItem.summa }, .... ]
  //    jossa lopputuloksena ovat olioissa kulutyypit ja niiden kokonaissumma.
  // reducer-funktio, joka annetaan parametrina alempana Reduce()-funktiolle (ks. Javascript-dokumentaatio)
  const reducer = (groupedData, currentItem) => {
    // haetaan index kulutyypin mukaan groupedData-taulukosta
    const index = groupedData.findIndex( item => item.tyyppi === currentItem.tyyppi  );
    // jos index löytyy eli kulutyyppi on jo groupedData-taulukossa
    if ( index >= 0 ) {
      // lisätään kulutyypin kokonaissummaa
      groupedData[index].summa = groupedData[index].summa + currentItem.summa;
    } else {
      // kulutyyppiä ei vielä ole -> lisätään groupedDatan loppuun uusi kulutyyppi ja summa:
      groupedData.push( { tyyppi: currentItem.tyyppi, summa: currentItem.summa } );
    }
    // palautetaan groupedData-taulukko:
    return groupedData;
  }

  // Ryhmitelty groupedData data-taulukko:
  // Alkuperäisestä kululaskujen Data-taulukosta haetaan reduce()-funktiolla kululaskut
  // taulukkoon groupedData ryhmiteltyinä kulutyypin ja sen kokonaissumman mukaan.
  // Saa alkuparametrina aiemmin määritetyn 'reducer'-funktion sekä tyhjän taulukon:
  let groupedData = props.data.reduce( reducer, [] );

  // Varsinaisen Donitsi-kuvion data-määreet
  let doughnutData = {
    labels: groupedData.map( item => item.tyyppi ),
    datasets: [
      {
        data: groupedData.map( item => item.summa ),
        // käytetään hsl-väriä: hsl(luku, x%, y%), jossa luku välillä 0-360 ja loput prosentteja (ks. JS-dokum.).
        // generoidaan luku välille 0-359 stringHash():llä käyttämällä jakojäännöstä 360:
        backgroundColor: groupedData.map( item => "hsl(" + (stringHash(item.tyyppi) % 360) + ", 80%, 70% )" ),
      }
    ]
  }

  // ******* Line-kuvio:
  // Piirretään kulut päivämäärän (x-akseli) ja summan (y-akseli) mukaan.
  // Luodaan taulukko linedata propsin kautta tulleesta data-taulukosta mäppäämällä
  // Huom: kohta 'item => ( {  } )': palautuksessa pitää olla () sulut {} ympärillä. Muuten palauttaa
  // Javascript lohkon!
  let linedata = props.data.map( item => ( { x: item.maksupaiva, y: item.summa } ) );

  // luodaan data-datasets Line-kuvioon
  let data = {            // dataoliomuuttuja
    datasets: [           // datasets-taulukko
      {                   // yksittäinen dataolio
        label: "kulut",
        data: linedata,
        fill: false,
        backgroundColor: 'rgba(0, 0, 0, 0.2)',
        borderColor: 'rgba(0, 0, 0, 0.1)'
      }
    ]
  }

  // lisämääreet Line-kuviolle (ks. specsit netissä)
  let options = {
    responsive: true,  // jotta vaikuttaa, pitää alla oleva <Line ...> olla <div..>:n sisällä ja sille css-määre stats__graph
    maintainAspectRatio: false,
    scales: {
      xAxes: [
        {
          type: "time",
          time: {
            displayFormats: {
              day: 'D.M.Y',
              month: 'M.Y'
            }
          }
        }
      ]
    }
  }

  return (
    <Content>
      <div className="stats">
        <h2>Tilastot</h2>
        <h3>Aikajanan kulut</h3>
        <div className="stats__graph">
          <Line data={data} options={options} />
        </div>
        <h3>Kulut tyypeittäin</h3>
        <div className="stats__graph">
          <Doughnut data={doughnutData} />
        </div>
      </div>
    </Content>
  );
}

export default Stats;