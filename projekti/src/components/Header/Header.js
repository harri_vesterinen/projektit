import React from 'react';
import './Header.css';

// Header: toteutus funktiokomponenttina, jos ei tarvita state:a
function Header(props) {
    return (
      <div className="header">
        <h1>Kulukirjanpito</h1>
      </div>
    );
}

export default Header;