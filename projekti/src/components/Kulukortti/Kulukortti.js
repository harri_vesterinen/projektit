import React from 'react';
import './Kulukortti.css';
import moment from 'moment'; // yarnilla asennettu Moment.js -paketti
import ArrowRight from '@material-ui/icons/ArrowRight'; // kulukortin muokkausnappi
import { Link } from 'react-router-dom'; // muokkausnapin reititys muokkauslomakkeelle

// Kulukortti: toteutus funktiokomponenttina, jos ei tarvita state:a
// propseissa tulee Items-komponentilta yksittäisen kulukortin tiedot 'data':na (props.data.)
function Kulukortti(props) {

  // käytetään yarnilla asennettua Moment.js -pakettia päivämäärien muotoilussa
  let maksupaiva = moment( props.data.maksupaiva );
  let kausi = ""; // laskutuskausimuuttuja
  let keskiarvo; // laskun keskiarvo / kk tietyllä ajanjaksolla

  // jos laskun kaudenalku ja kaudenloppu ovat tiedossa:
  if( props.data.kaudenalku && props.data.kaudenloppu ) {
    // muunnetaan ensin pvm:t moment-muotoon:
    let kaudenalku = moment(props.data.kaudenalku);
    let kaudenloppu = moment(props.data.kaudenloppu);
    // näytölle tuleva kausitieto
    kausi = kaudenalku.format("D.M.Y") + " - " + kaudenloppu.format("D.M.Y");
    // lasketaan laskun summan kk-keskiarvo:
    // - ensin päivien lkm kaudenloppu ja kaudenalku välillä
    //      ks. https://momentjs.com/docs/#/displaying/difference/
    let paivat = kaudenloppu.diff( kaudenalku, 'days' );
    // lasketaan per kk
    keskiarvo = props.data.summa / paivat * 30;

  }

  return (
    <div className="kulukortti">
      <div className="kulukortti__ryhma">
        <div className="kulukortti__rivi">
          <div className="kulukortti__tyyppi">{props.data.tyyppi}</div>
          <div className="kulukortti__summa">{props.data.summa.toFixed(2)} €</div>
        </div>
        <div className="kulukortti__rivi">
          <div className="kulukortti__maksupaiva">{maksupaiva.format("D.M.Y")}</div>
          <div className="kulukortti__ajanjakso">{kausi}</div>
        </div>
        <div className="kulukortti__rivi">
          <div className="kulukortti__laskuttaja">{props.data.saaja}</div>
          <div className="kulukortti__keskiarvo">{keskiarvo ? keskiarvo.toFixed(2) + ' € / kk' : ''}</div>
        </div>
      </div>
      <div className="kulukortti__linkki">
        <Link to={ "/edit/" + props.data.id }><ArrowRight /></Link>
      </div>
    </div>
    
  );
}

export default Kulukortti;