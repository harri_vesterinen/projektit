import React from 'react';

import Content from '../Content/Content';
import ItemForm from '../ItemForm/ItemForm';

import './EditItem.css';

// Tälle EditItem-komponentille siirrytään etusivun muokkausnuolilinkillä
// syöttölomake on omassa ItemForm-komponentissaan

function EditItem(props) {

    // haetaan props.data-taulukosta sen kululomakkeen indeksi, jonka id-tieto
    // on sama, kuin URL-parametrina muokkaustoiminnossa välitetty id-tieto
    // (eli näin saadaan kiinni se lasku, jota ollaan muokkaamassa)
    const index = props.data.findIndex( item => item.id === props.match.params.id );

    // haetaan indexillä props.data -taulukosta oikea laskutieto, joka
    // välitetään alempana <ItemForm>-komponentille props-parametrina
    let itemData = props.data[index];

    return (
      <Content>
        <div className="edititem">

            <h2>Kulun muokkaaminen</h2>
            <ItemForm onFormSubmit={props.onFormSubmit} 
                      selectList={props.selectList}
                      data={itemData}
                      onDeleteItem={props.onDeleteItem} />

        </div>
      </Content>
    );
  }

  export default EditItem;