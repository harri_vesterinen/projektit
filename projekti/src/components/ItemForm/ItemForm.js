import React from 'react';
import Button from '../buttons';
// Selainhistorian saaminen mukaan withRouterilla
import { withRouter } from 'react-router';
// Yksilöivän tunnisteen generointiin tarkoitettu uuid mukaan
import uuid from 'uuid';

import './ItemForm.css';

// Muistiksi:
// - Reactissa <form ..> ei tarvitse normaalin HTML:n tapaan parametreja (ks html-dokumentaatio)
// - kenttäniminä (name) on hyvä käyttää selvyyden vuoksi samoja, kuin mitä on testdata.js:ssä
// - syöttökentissä annettu osassa size="10" parametrina, jotta kentät pysyvät määritetyn lomakkeen
//     leveydessä 
//    input ja select -kentille määre width: 100% / AddItem.css:ssä (.itemform input, .itemform select)
//      -> asettuvat tasaisesti eikä yli reunan.
// esim: <input type="text" name="summa" size="10" />

class ItemForm extends React.Component {

    constructor(props) {
        super(props);
        // jos data-taulukko tulee propsina -> käytä sitä, muutoin
        // laita data-taulukkoon tyhjät oletusarvot:
        const data = props.data ? props.data : {
            tyyppi: "Vesi",
            summa: 0,
            maksupaiva: "",
            kaudenalku: "",
            kaudenloppu: "",
            saaja: ""
        } 
        // aseta state-muuttujaan data yo. ehdon mukaan
        this.state = {
            data: data
        };

        // ao. rivi kopioitu paikasta: https://reactjs.org/docs/forms.html
        // kohdasta 'Handling Multiple Inputs'
        this.handleInputChange = this.handleInputChange.bind(this);

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
        this.handleDeleteItem = this.handleDeleteItem.bind(this);
    }

    // ao. funktio kopioitu paikasta: https://reactjs.org/docs/forms.html
    // kohdasta 'Handling Multiple Inputs'
    // funktio selvittää muutetun kentän arvon ja nimen ja muuttaa ko. arvoa
    // state-muuttujan kenttään. Muokattu vain niin, että päivittää
    // state-muuttujan sisällä DATA-taulukon sisällä olevia tietoja.
    //    (pitää bindata construktorissa)
    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
    
        this.setState({
            data: {             // tämä rivi lisätty
                // ao. tarkoittaa, että ottaa mukaan state-muutokseen data-taulukon,
                // ja että sen sisältö puretaan auki. Lisäksi taulukon perään tulee
                // vielä [name]: value -kohdassa uusi arvo.
                ...this.state.data,     // tämä rivi lisätty
                [name]: value
            }                   // tämä rivi lisätty
        });
    }

    // PERUUTA-napin toiminto (pitää bindata construktorissa)
    handleCancel(event) {
        event.preventDefault();
        this.props.history.goBack();
    }

    // LISÄÄ-napin toiminto (pitää bindata construktorissa)
    handleSubmit(event) {
        event.preventDefault(); // estetään Submitin oletustoiminto, jossa kenttäarvot
        // menisivät Submit-painamisen jälkeen selaimen osoiterivillä parametreina, 
        // ja selain jäisi ko. osoiteriville esim:
        // http://localhost:3000/add?tyyppi=Vakuutus&summa=10&maksupaiva=2019-04-17&kaudenalku=2019-04-07&kaudenloppu=2019-04-20&saaja=Fortum
        
        // varmistetaan, että datan tiedot ovat oikein.
        // esim. lomakkeen summa tulee tekstimuotoisena, vaikka se on lomakkeella number
        // otetaan ensin kopio this.state.data -oliosta:
        let data = Object.assign({}, this.state.data);
        // muunnetaan data.summa Float-numeroksi:
        data.summa = parseFloat(data.summa);
        // luodaan uniikki id, ellei se ole jo olemassa. On olemassa, jos olemassaolevaa
        // laskua ollaan muokkaamassa
        data.id = data.id ? data.id : uuid.v4();
        // lähetetään tarkistettu data-taulukon kopio onFormSubmitina eteenpäin:
        this.props.onFormSubmit(data);
        // Lisätään historian loppuun etusivun osoite
        this.props.history.push("/");
    }

    // POISTA-napin toiminto (pitää bindata construktorissa)
    handleDeleteItem(event) {
        // estetään oletustoiminnallisuus
        event.preventDefault();
        // kutsutaan propsin onDeleteItem(..):ia
        this.props.onDeleteItem(this.state.data.id);
        // Lisätään historian loppuun etusivun osoite
        this.props.history.push("/");
    }

    render() {
        return(

            <form onSubmit={this.handleSubmit}>

                <div className="itemform">
                    <div className="itemform__row">
                        <div>
                            <label htmlFor="tyyppi">Kulutyyppi</label>
                            <select name="tyyppi" value={ this.state.data.tyyppi } onChange={this.handleInputChange}>
                                
                                { this.props.selectList.map( item => <option value={item} key={item}>{item}</option> ) }
                                
                            </select>
                        </div>
                    </div>

                    <div className="itemform__row">
                        <div>
                            <label htmlFor="summa">Summa</label>
                            <input type="number" name="summa" step="0.01" value={ this.state.data.summa } onChange={this.handleInputChange} />
                        </div>

                        <div>
                            <label htmlFor="maksupaiva">Maksupäivä</label>
                            <input type="date" name="maksupaiva" value={ this.state.data.maksupaiva } onChange={this.handleInputChange} />
                        </div>
                    </div>

                    <div className="itemform__row">
                        <div>
                            <label htmlFor="kaudenalku">Laskutuskauden alku</label>
                            <input type="date" name="kaudenalku" size="10" value={ this.state.data.kaudenalku } onChange={this.handleInputChange} />
                        </div>

                        <div>
                            <label htmlFor="kaudenloppu">Laskutuskauden loppu</label>
                            <input type="date" name="kaudenloppu" size="10" value={ this.state.data.kaudenloppu } onChange={this.handleInputChange} />
                        </div>
                    </div>

                    <div className="itemform__row">
                        <div>
                            <label htmlFor="saaja">Laskuttaja</label>
                            <input type="text" name="saaja" value={ this.state.data.saaja } onChange={this.handleInputChange} />
                        </div>
                    </div>

                    <div className="itemform__row">
                        <div>
                            <Button onClick={this.handleCancel}>PERUUTA</Button>
                        </div>
                        <div>
                            <Button type="submit" primary>{ this.state.data.id ? "TALLENNA" : "LISÄÄ" }</Button>
                        </div>
                    </div>
                 
                    { this.props.onDeleteItem ? 
                    <div className="itemform__row">
                        <div>
                            <Button onClick={ this.handleDeleteItem }>POISTA</Button>
                        </div>
                        <div></div>
                    </div> : "" }
                    
                </div>

            </form>

        );
    }

}

// withRouter palauttaa ItemFormin selainhistorian. Näin päästään mm. takaisin etusivulle
export default withRouter(ItemForm);