import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import './App.css';
import testdata from './testdata';

import Header from './components/Header/Header';
import Items from './components/Items/Items';
import Stats from './components/Stats/Stats';
import Settings from './components/Settings/Settings';
import Menu from './components/Menu/Menu';
import AddItem from './components/AddItem/AddItem';
import EditItem from './components/EditItem/EditItem';

class App extends Component {

  // tuodaan konstruktorissa state-muuttujaan testdata-taulukko data-muuttujaan,
  //   joka voidaan välittää alemman tason komponenteille propsina
  // Sama koskee selectList:iä. Se liittyy asetuksissa (Settings.js) olevaan
  //   Kulutyypin määrittelylistaan sekä AddItem.js:n kautta ItemForm.js:lle,
  //   jossa sitten näkyy valintalista.
  constructor(props) {
    super(props);
    this.state = {
      data: testdata,
      selectList: [ "Harrastukset", "Puhelin", "Sähkö", "Terveys", "Vero", "Vesi" ]
    }

    // muista alemmille komponenteille välitettävien funktioiden bindaus!
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
    this.handleSelectListForm = this.handleSelectListForm.bind(this);
    this.handleDeleteItem = this.handleDeleteItem.bind(this);

  }

  // funktio, jolla hoidetaan ItemForm.js-komponentin Submit-toiminnon (LISÄÄ) kautta
  // tulevan uuden laskun lisääminen Data-taulukkoon.
  handleFormSubmit(newdata) {
    let storeddata = this.state.data.slice(); // otetaan uusi kopio State-muuttujan data-taulukosta
    // onko newdatan id jo olemassa storeddata-taulukon id-tiedoissa?
    // jos on, kyseessä olemassaolevan laskun muokkaus, muutoin uuden lisääminen
    const index = storeddata.findIndex( item => item.id === newdata.id );
    // jos löytyy, palautuu indexiin arvo >= 0
    if( index >= 0 ) {
      storeddata[index] = newdata; // korvataan olemassaoleva lasku newdatalla
    } else {
      storeddata.push(newdata); // lisätään newdata storeddatan loppuun
    }
    
    // sortataan taulukko. Annettava .sort-funktiolle parametrina sorttausfunktio,
    // koska kyseessä on oliotaulukko (pelkkä .sort ei toimi oliotaulukossa, koska
    // sortti ei tiedä, minkä mukaan pitäisi sortata)
    storeddata.sort( (a, b) => { 
        const aDate = new Date(a.maksupaiva); // muunnetaan maksupäivä (tekstiä) Dateksi
        const bDate = new Date(b.maksupaiva); // muunnetaan maksupäivä (tekstiä) Dateksi
        // ks. JavaScriptin opas ao. riviin liittyen (jos palauttaa negat. / posit., miten järjestyy)
        return bDate.getTime() - aDate.getTime(); // getTime muuttaa millisekunneiksi (numero)
     } );
    this.setState({
      data: storeddata // asetetaan This.state.data:an uusi storeddatan arvo
    });
  }

  // funktio, jolla hoidetaan Settings.js -komponentin Submit-toiminnon (LISÄÄ) kautta
  // tulevan uuden kulutyypin lisääminen selectListin (kulutyypit) listaan.
  handleSelectListForm(newitem) {
    // otetaan kopio staten selectList:stä
    let selectList = this.state.selectList.slice();
    // lisätään loppuun newitem
    selectList.push(newitem);
    // sortataan lista: ei tarvita nyt sorttausfunktiota sort() sisälle, koska lista on järjestettävissä
    //  sellaisenaan aakkosjärjestykseen (listassa on vain taulukossa yksittäisiä arvoja,
    //  ei oliomuuttujia)
    selectList.sort();
    // päivitetään state-muuttuja
    this.setState({
      selectList: selectList
    });
  }

  // funktio, jolla poistetaan kulukortti. Parametrina poistettavan kortin id
  // Tämä funktio välitetään propsina komponentille <EditItem>
  handleDeleteItem(id) {
    let storeddata = this.state.data.slice(); // otetaan uusi kopio State-muuttujan data-taulukosta
    // filtteröidään storeddatasta pois se kulukortti, jonka id on sama,
    // kuin parametrina annettu id-tieto. Eli filter() -funktio palauttaa taulukon,
    // jonka elementit menevät läpi ehdosta: item.id !== id (ks. JS-dokumentaatio)
    storeddata = storeddata.filter( item => item.id !== id );
    this.setState({
      data: storeddata // asetetaan this.state.data:an uusi storeddatan arvo
    });
  }

  // Huomaa alla <EditItem>-komponentin renderöinnissä mukana (props) parametrina. Se
  // tarvitaan, jotta URL-parametrit voidaan välittää EditItem-komponentille (esim. :id)
  // Huomaa myös spread operaattori {...props}, kun propsit välitetään avattuina <EditItem>:lle
  render() {
    return (
      <Router>
        <div className="App">
          <Header />
          <Route path="/" exact render={ () => <Items data={this.state.data} /> } />
          <Route path="/stats" render={ () => <Stats data={this.state.data} /> } />
          <Route path="/settings" render={ () => <Settings selectList={this.state.selectList} onFormSubmit={this.handleSelectListForm} /> } />
          <Route path="/add" render={ () => <AddItem onFormSubmit={this.handleFormSubmit} selectList={this.state.selectList} /> } />
          <Route path="/edit/:id" render={ (props) => <EditItem data={this.state.data} 
                                                                selectList={this.state.selectList}                                                                
                                                                onFormSubmit={this.handleFormSubmit}
                                                                onDeleteItem={this.handleDeleteItem}
                                                                {...props} /> } />
          <Menu />
        </div>
      </Router>
    );
  }
}

export default App;
